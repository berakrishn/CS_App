from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from . import views

app_name = 'ColorSurvey'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout,name='logout'),
    url(r'^language_view/$',views.language_view, name='language_view'),
    url(r'^language/add/$', views.language_add,name='language_add'),
    url(r'^language/edit/(?P<language_id>\d+)/$',views.language_edit,name='language_edit'),
    url(r'^language/delete/(?P<language_id>\d+)/$',views.language_delete,name='language_delete'),
    url(r'^language_profile/(?P<language_id>\d+)/$',views.language_profile,name='language_profile'),
    url(r'^color_terms/list/(?P<language_id>\d+)/$',views.list_color_terms,name='list_color_terms'),
    url(r'^color_terms/edit/(?P<color_term_id>\d+)/$',views.edit_color_terms,name='edit_color_terms'),
    url(r'^color_terms/delete/(?P<color_term_id>\d+)/$',views.delete_color_terms,name='delete_color_terms'),
    url(r'^color_terms/survey/home$',views.survey_home, name='survey_home'),
    url(r'^color_terms/survey/start/(?P<language_id>\d+)/(?P<color_id>\d+)$',views.color_terms_survey_startpage,name='color_terms_survey_startpage'),
    url(r'^color_terms/survey/(?P<language_id>\d+)/(?P<color_id>\d+)$',views.color_terms_survey,name='color_terms_survey'),
    url(r'^color_terms/survey/end$',views.survey_end, name='survey_end'),
    url(r'^color_terms/add/(?P<language_id>\d+)/$',views.add_color_terms,name='add_color_terms'),
    url(r'^color_db_data/add/$',views.color_db_data, name='color_db_data'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
