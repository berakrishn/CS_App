# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ColorSurvey.models import UserProfile
from django.contrib import admin
from django.db import models
from .models import color_term, language, color_data, survey_data

admin.site.register(UserProfile)
admin.site.register(language)
admin.site.register(color_term)
admin.site.register(color_data)
admin.site.register(survey_data)
