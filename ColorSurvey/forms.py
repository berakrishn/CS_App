from ColorSurvey.models import UserProfile
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django import forms
from .models import color_term, language, color_data

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')

    def clean_confirm_password(self):
        password = self.cleaned_data['password']
        confirm_password = self.cleaned_data['confirm_password']
        if not password or not confirm_password:
            raise ValidationError("Invalid password")
        if password != confirm_password:
            raise ValidationError("Password confirmation failed")
        return confirm_password

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('website',)

class LanguageForm(forms.ModelForm):
    class Meta:
        model = language
        fields = ('l_name',)
        labels = {"l_name": "Language Name"}

class ColorTermForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ColorTermForm, self).__init__(*args, **kwargs)
        self.fields['seed_color'].widget.attrs['readonly'] = True
    class Meta:
        model = color_term
        fields = ('color','meaning','seed_color')

class ColorDataForm(forms.ModelForm):
    class Meta:
        model = color_data
        fields = ('value_lab','value_rgb','color_tile')
