# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import validate_comma_separated_integer_list

class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)
    # The additional attributes we wish to include.
    website = models.URLField(blank=True)
    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username

class language(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    l_name = models.CharField(max_length = 100, null = False, unique = True)
    def __unicode__(self):
      return self.l_name

class color_term(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    language_id = models.ForeignKey(language, on_delete=models.CASCADE)
    color = models.CharField(max_length=100, null = False, unique = True)
    meaning = models.TextField(blank = False)
    seed_color = models.CharField(max_length=100)
    value_lab = models.CharField(max_length=100, blank = True, null = True, validators=[validate_comma_separated_integer_list])

    def __unicode__(self):
      return self.color

class color_data(models.Model):
    value_lab = models.CharField(max_length=100, null = False, unique = True, validators=[validate_comma_separated_integer_list])
    value_rgb = models.CharField(max_length=100, null = False, unique = True, validators=[validate_comma_separated_integer_list])
    color_tile = models.ImageField('img',upload_to='ColorData/')

class survey_data(models.Model):
    user_id = models.ForeignKey(User,on_delete = models.CASCADE)
    language_id = models.ForeignKey(language, on_delete = models.CASCADE)
    color_term_id = models.ForeignKey(color_term, null = True, on_delete = models.CASCADE)
    selected_colors = models.CharField(max_length=200, null = True, validators=[validate_comma_separated_integer_list])
