# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse,HttpResponseRedirect
from django.template import loader,RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib import auth, messages
from django.shortcuts import get_object_or_404, render
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from skimage import color
from django.core.files import File
import os, urllib, scipy, numpy, ast

from ColorSurvey.models import language, color_term, color_data, survey_data
from ColorSurvey.forms import UserForm, UserProfileForm, LanguageForm, ColorTermForm, ColorDataForm

from views_user_auth import *


'''
==============================================================================
Methods for all the functional features of the website
==============================================================================
'''

@login_required(login_url='ColorSurvey:login')
def index(request):
    if request.user.is_authenticated():
        login_status = 1
    else:
        login_status = 0
    template = loader.get_template('index.html')
    context = {'login_status':login_status}
    return HttpResponse(template.render(context, request))

@login_required(login_url='ColorSurvey:login')
def language_add(request):
    current_user = request.user
    if request.method=='POST':
        form = LanguageForm(request.POST)
        if form.is_valid():
            new_language = form.save(commit=False)
            new_language.user_id = request.user
            new_language.save()
            return HttpResponseRedirect(reverse('ColorSurvey:language_view'))
    else:
        form = LanguageForm()
    context = {'form':form}
    template = loader.get_template('language_add.html')
    return HttpResponse(template.render(context, request))

@login_required(login_url='ColorSurvey:login')
def language_edit(request,language_id):
    context = {}
    language_obj = get_object_or_404(language, pk = language_id)
    if request.method=='GET':
        form = LanguageForm(instance=language_obj)
        context = {'form':form}
    else:
        form = LanguageForm(request.POST, instance = language_obj)
        if form.is_valid():
            language_obj = form.save(commit = False)
            language_obj.save()
            return HttpResponseRedirect(reverse('ColorSurvey:language_view'))
    template = loader.get_template('language_edit.html')
    return HttpResponse(template.render(context,request))

@login_required(login_url='ColorSurvey:login')
def language_delete(request,language_id):
    language_obj = get_object_or_404(language,pk=language_id)
    language_obj.delete()
    return HttpResponseRedirect(reverse('ColorSurvey:language_view'))

@login_required(login_url='ColorSurvey:login')
def language_view(request):
    current_user = request.user
    obj = language.objects.filter(user_id=current_user)
    context = {'language_list':obj}
    template = loader.get_template('language_view.html')
    return HttpResponse(template.render(context,request))

@login_required(login_url='ColorSurvey:login')
def language_profile(request,language_id):
    current_user = request.user
    temp_lang_id = int(language_id)
    language_obj = language.objects.get(id = temp_lang_id)
    total_terms = color_term.objects.filter(user_id = current_user).filter(language_id = temp_lang_id).count()
    context = {'language_obj':language_obj,'total_terms':total_terms}
    template = loader.get_template('language_profile.html')
    return HttpResponse(template.render(context, request))

@login_required(login_url='ColorSurvey:login')
def list_color_terms(request,language_id):
    current_user = request.user
    temp_lang_id = int(language_id)
    try:
        obj = language.objects.filter(user_id = current_user).get(id = temp_lang_id)
    except language.DoesNotExist:
        obj = None
        user_languages = language.objects.filter(user_id = current_user)
        context = {'user_languages':user_languages}
        template = loader.get_template('error.html')
        return HttpResponse(template.render(context, request))

    color_term_obj = color_term.objects.filter(user_id = current_user).filter(language_id = temp_lang_id)

    context = {"color_terms_list":color_term_obj}
    template = loader.get_template('list_color_terms.html')
    return HttpResponse(template.render(context,request))

@login_required(login_url='ColorSurvey:login')
def add_color_terms(request,language_id):
    current_user = request.user
    form = ColorTermForm(request.POST)
    temp_lang_id = int(language_id)

    try:
        obj = language.objects.filter(user_id = current_user).get(id = temp_lang_id)
    except language.DoesNotExist:
        obj = None
        user_languages = language.objects.filter(user_id = current_user)
        context = {'user_languages':user_languages}
        template = loader.get_template('error.html')
        return HttpResponse(template.render(context, request))

    if request.method=='POST':
        if form.is_valid():
            new_color_term = form.save(commit=False)
            new_color_term.user_id = request.user

            seed_color_rgb_raw = new_color_term.seed_color #value of seed_color_rgb_raw is #ABCDEF
            seed_color_rgb_raw = list(seed_color_rgb_raw) #value of seed_color_rgb_raw is [#,A,B,C...]

            rgb_color = [[[0.0,0.0,0.0]]]
            j = 0
            result_hex_rgb = []
            # This for loog tries to get "AB","CD","EF" from [#,A,B,C...] as corressponding RGB values
            for i in range(1,6,2):
                s = ""
                # extracting 'A' and 'B' from the array [#,A,B,C...]
                seq = (seed_color_rgb_raw[i],seed_color_rgb_raw[i+1])
                # joining unicode 'A' and 'B' as string 'AB' and appending it to result_hex_rgb
                result_hex_rgb.append(str(s.join(seq)))
                rgb_color[0][0][j] = float(int(result_hex_rgb[j],16))/256
                j = j + 1

            lab_color = color.rgb2lab(rgb_color)
            new_color_term.value_lab = [lab_color[0][0][0], lab_color[0][0][1], lab_color[0][0][2]]

            new_color_term.language_id = obj
            new_color_term.save()
            return HttpResponseRedirect(reverse('ColorSurvey:add_color_terms',args=[language_id,]))
    else:
        form = ColorTermForm()
    context = {'form':form}
    template = loader.get_template('color_terms.html')
    return HttpResponse(template.render(context, request))

@login_required(login_url='ColorSurvey:login')
def edit_color_terms(request,color_term_id):
    current_user = request.user
    form = ColorTermForm(request.POST)
    context = {}
    color_term_object = get_object_or_404(color_term, pk = color_term_id)
    if request.method=='GET':
        form = ColorTermForm(instance = color_term_object)
        initial_color = color_term_object.seed_color
        print form
        context = {'form':form, 'initial_color':initial_color}
    else:
        form = ColorTermForm(request.POST, instance = color_term_object)
        if form.is_valid():
            color_term_object = form.save(commit = False)
            seed_color_rgb_raw = color_term_object.seed_color #value of seed_color_rgb_raw is #ABCDEF
            seed_color_rgb_raw = list(seed_color_rgb_raw) #value of seed_color_rgb_raw is [#,A,B,C...]

            rgb_color = [[[0.0,0.0,0.0]]]
            j = 0
            result_hex_rgb = []
            # This for loog tries to get "AB","CD","EF" from [#,A,B,C...] as corressponding RGB values
            for i in range(1,6,2):
                s = ""
                # extracting 'A' and 'B' from the array [#,A,B,C...]
                seq = (seed_color_rgb_raw[i],seed_color_rgb_raw[i+1])
                # joining unicode 'A' and 'B' as string 'AB' and appending it to result_hex_rgb
                result_hex_rgb.append(str(s.join(seq)))
                rgb_color[0][0][j] = float(int(result_hex_rgb[j],16))/256
                j = j + 1

            lab_color = color.rgb2lab(rgb_color)
            color_term_object.value_lab = [lab_color[0][0][0], lab_color[0][0][1], lab_color[0][0][2]]

            color_term_object.save()
            language_id = color_term_object.language_id.id
            return HttpResponseRedirect(reverse('ColorSurvey:list_color_terms',args=[language_id,]))
    template = loader.get_template('color_terms_edit.html')
    return HttpResponse(template.render(context,request))

@login_required(login_url='ColorSurvey:login')
def delete_color_terms(request,color_term_id):
    color_term_object = get_object_or_404(color_term,pk=color_term_id)
    language_id = color_term_object.language_id.id
    color_term_object.delete()
    return HttpResponseRedirect(reverse('ColorSurvey:list_color_terms',args=[language_id,]))

'''
==============================================================================
************************** DO NOT USE THIS FUNCTION **************************
This function is used for feeding the data-set of 5572 color-tiles in the
database model named "color_data"
==============================================================================
'''

@login_required(login_url='ColorSurvey:login')
def color_db_data(request):
    #This function feeds the dataset of all 5572 colors into the sql database.
    f_rgb = open('ColorData/rgb1.txt')
    line_rgb = f_rgb.readline()

    f_lab = open('ColorData/lab.txt')
    line_lab = f_lab.readline()
    i = 1

    # The os.listdir() returns random ordered list. To have ordered list: 1.jpg, 2.jpg, 3.jpg ...,
    # we have to first convert unicode list to string list and then use sorted() function
    sorted_dir_list = sorted(os.listdir("ColorData/"))
    sorted_dir_list = [str(x) for x in sorted_dir_list]
    sorted_dir_list = sorted(sorted_dir_list, key = len)

    for file in sorted_dir_list:
        if file.endswith(".jpg") and i <= 5572:
            print i, file
            line_rgb = [ float(x) for x in line_rgb.split()]
            obj = color_data.objects.create()
            obj.value_rgb = [0,0,0]
            obj.value_rgb[0] = line_rgb[0]
            obj.value_rgb[1] = line_rgb[1]
            obj.value_rgb[2] = line_rgb[2]

            temp = [[[]]]
            temp[0][0][:] = obj.value_rgb
            tp = color.rgb2lab(temp)
            tp = tp.tolist()
            obj.value_lab = tp[0][0]
            image_path = 'ColorData/' + str(i) + '.jpg'
            obj.color_tile = image_path
            obj.save()

            line_rgb = f_rgb.readline()
            line_lab = f_lab.readline()
            i = i + 1
    f_rgb.close()
    f_lab.close()

    context = {}
    template = loader.get_template('color_data.html')
    return HttpResponse(template.render(context, request))

'''
==============================================================================
Color Survey functions and methods
==============================================================================
'''

def find_k_nearest_colors(k,seed):
    seed = ast.literal_eval(seed)
    color_list = color_data.objects.all()
    options_list = []
    for i in color_list:
        temp = [None, None, None, None, None]
        i.value_lab = ast.literal_eval(i.value_lab)
        expression = pow((pow((seed[0]-i.value_lab[0]),2) + pow((seed[1]-i.value_lab[1]),2) + pow((seed[2]-i.value_lab[2]),2)),0.5)
        if expression < 40:
            temp[0] = i.id
            temp[1] = expression
            temp[2] = float(i.value_lab[0])
            temp[3] = float(i.value_lab[1])
            temp[4] = float(i.value_lab[2])
            options_list.append(temp)

    # Now sort the resultant 2-D array (options_list) by its second-column(the perceptual distance)
    options_list = sorted(options_list,key=lambda x: x[1])
    # If options_list has more than 'k' elements, only take first k elements
    tp = []
    if len(options_list) > k:
        count = 0
        for itr in options_list:
            if count < k:
                tp.append(itr)
            count += 1
    options_list = tp
    # Sort the colors in order of their lightness value (value_lab[0]) for uniform variation in 'a' & 'b'
    # This makes display of choices more uniform in both the axes.
    options_list = sorted(options_list,key=lambda x: (x[2]), reverse=True)
    #print "\n\nOPTIONS LIST\n",options_list, " >>>> ", len(options_list)
    return options_list

@login_required(login_url='ColorSurvey:login')
def color_terms_survey(request,language_id,color_id):
    current_user = request.user
    temp_lang_id = int(language_id)

    color_terms_list = color_term.objects.filter(user_id = current_user).filter(language_id = temp_lang_id)
    tp = []
    for itr in color_terms_list:
        tp.append(int(itr.id))
    color_terms_list = tp

    index = color_terms_list.index(int(color_id))
    if index == 0:
        print "0"
        request.session['index'] = 0
        request.session['next_color_id'] = color_terms_list[request.session['index']+1]
        request.session['flag'] = 0
        request.session['last_term_index'] = color_terms_list[len(color_terms_list)-1]

    if request.method=='POST':

        checkbox_ticked = request.POST.getlist('tag')

        # Get the language object for reference for the ForeignKey
        tp = language.objects.get(id = int(temp_lang_id))
        new_survey = survey_data.objects.create(user_id = current_user, language_id = tp)

        # Get the color_term object for reference for the ForeignKey
        tp = color_term.objects.get(id = int(color_id))
        new_survey.color_term_id = tp

        # Convert unicode list (checkbox_ticked) to integer list (selected_colors_list)
        selected_colors_list = []
        for itr in checkbox_ticked:
            tp = int(itr)
            selected_colors_list.append(tp)

        new_survey.selected_colors = selected_colors_list
        new_survey.save()

        if color_terms_list[request.session['index']] == request.session['last_term_index']:
            request.session['flag'] = 1
            context = {}
            template = loader.get_template('survey_end.html')
            return HttpResponse(template.render(context,request))
        else:
            request.session['index'] += 1
            if color_terms_list[request.session['index']] == request.session['last_term_index']:
                request.session['flag'] = 1
            else:
                request.session['next_color_id'] = color_terms_list[request.session['index']+1]

    # Retrieve Color Term Object from database of all color terms
    color_term_object = color_term.objects.get(id = int(color_id))

    k_nearest_colors = find_k_nearest_colors(96, color_term_object.value_lab)

    options_list = []
    for itr in k_nearest_colors:
        tp = color_data.objects.get(id = int(itr[0]))
        options_list.append(tp)

    context = {'temp_lang_id':temp_lang_id, 'flag': request.session['flag'], 'next_color_id':request.session['next_color_id'], 'options_list':options_list, 'color_term_object':color_term_object}
    template = loader.get_template('color_terms_survey.html')
    return HttpResponse(template.render(context,request))

@login_required(login_url='ColorSurvey:login')
def survey_home(request):
    current_user = request.user
    obj = language.objects.filter(user_id=current_user)
    survey_list_lang_id = []
    survey_list_lang_name = []
    survey_list_color_id = []
    for itr in obj:
        color_terms_list = color_term.objects.filter(user_id=current_user).filter(language_id=itr.id)
        for each in color_terms_list:
            tp = [None,None,None]
            tp[0] = int(itr.id)
            tp[1] = itr.l_name
            tp[2] = int(each.id)
            survey_list_lang_id.append(tp[0])
            survey_list_lang_name.append(tp[1])
            survey_list_color_id.append(tp[2])
            break
    survey_list = zip(survey_list_lang_id, survey_list_lang_name, survey_list_color_id)
    context = {'survey_list':survey_list}
    template = loader.get_template('survey_home.html')
    return HttpResponse(template.render(context,request))

@login_required(login_url='ColorSurvey:login')
def color_terms_survey_startpage(request,language_id,color_id):
    language_id = int(language_id)
    color_id = int(color_id)
    context = {'language_id':language_id , 'color_id':color_id}
    template = loader.get_template('color_terms_survey_startpage.html')
    return HttpResponse(template.render(context, request))

@login_required(login_url='ColorSurvey:login')
def survey_end(request):
    current_user = request.user
    context = {}
    template = loader.get_template('survey_end.html')
    return HttpResponse(template.render(context,request))
